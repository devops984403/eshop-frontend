import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NzCardComponent, NzCardMetaComponent, NzCardModule} from "ng-zorro-antd/card";
import {Product} from "../../model";
import {CurrencyPipe, JsonPipe, NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzModalComponent, NzModalModule} from "ng-zorro-antd/modal";

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [
    NzCardModule,
    NgIf,
    NzIconModule,
    NzButtonModule,
    NzToolTipModule,
    NgOptimizedImage,
    NzModalModule,
    CurrencyPipe,
    JsonPipe,
    NgForOf,
  ],
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent {

  @Input() product!: Product
  @Output() addProductToCart = new EventEmitter<Product>();

  isVisible = false;
  addToCart() {
    this.addProductToCart.emit(this.product);
  }

}
