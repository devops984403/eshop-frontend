import {Component, DestroyRef, OnInit, inject} from '@angular/core';
import {NzInputGroupComponent, NzInputModule} from "ng-zorro-antd/input";
import {NzContentComponent, NzHeaderComponent, NzLayoutComponent, NzSiderComponent} from "ng-zorro-antd/layout";
import {NzIconDirective, NzIconModule} from "ng-zorro-antd/icon";
import {NzMenuDirective, NzMenuItemComponent, NzMenuModule, NzSubMenuComponent} from "ng-zorro-antd/menu";
import {RouterOutlet} from "@angular/router";
import {ProductService} from "../service/product.service";
import {CartItem, Page, Product, ProductSearch} from "../model";
import {ProductComponent} from "../component/product/product.component";
import { NzSliderModule } from 'ng-zorro-antd/slider';
import {CurrencyPipe, NgForOf, NgIf} from "@angular/common";
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop'
import { debounceTime, distinctUntilChanged, finalize, switchMap, take, tap } from 'rxjs';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import {NzCardComponent} from "ng-zorro-antd/card";
import {NzFormControlComponent, NzFormItemComponent, NzFormLabelComponent} from "ng-zorro-antd/form";
import {NzColDirective} from "ng-zorro-antd/grid";
import {NzInputNumberComponent} from "ng-zorro-antd/input-number";
import {NzMessageService} from "ng-zorro-antd/message";
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import {NzBadgeComponent} from "ng-zorro-antd/badge";
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {NzListComponent} from "ng-zorro-antd/list";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    NzToolTipModule,
    NzDrawerModule,
    NzIconModule,
    NzButtonModule,
    NzSliderModule,
    NzSpinModule,
    NzPaginationModule,
    NzInputGroupComponent,
    NzContentComponent,
    NzHeaderComponent,
    NzIconDirective,
    NzLayoutComponent,
    NzMenuModule,
    NzMenuDirective,
    NzMenuItemComponent,
    NzSiderComponent,
    NzSubMenuComponent,
    RouterOutlet,
    NzInputModule,
    ProductComponent,
    FormsModule,
    ReactiveFormsModule,
    NgForOf,
    NzModalModule,
    NzCardComponent,
    NzFormItemComponent,
    NzFormLabelComponent,
    NzFormControlComponent,
    NzColDirective,
    NzInputNumberComponent,
    NgIf,
    NzBadgeComponent,
    CurrencyPipe,
    NzListComponent
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {

  products: Product[] = []
  pageInfo: Page = new Page()
  totalElements = 0
  offset = 1
  searchParams = this.fb.group({
    name: [null],
    description: [null],
    price_max: [null],
    price_min: [null],
    tag_line: [null],
    food_pairing: [null]
  })

  priceRange = [0, 20]

  loading = true
  cartVisible = false
  cartItems: CartItem[]
  totalPriceInCart = 0

  private destroyRef = inject(DestroyRef)

  constructor(
    private productService: ProductService,
    private fb: FormBuilder,
    private message: NzMessageService
    ) {
    this.cartItems = JSON.parse(localStorage.getItem("cartItems") as string) as CartItem[];
    if (!this.cartItems) {
      this.cartItems = []
    } else {
      this.calculateTotalPriceInCart()
    }
  }

  ngOnInit() {
    this.searchProduct$(this.searchParams.value as ProductSearch).subscribe();

    this.searchParams.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged((a, b) => JSON.stringify(a) === JSON.stringify(b)),
      switchMap((value) => this.searchProduct$(value as ProductSearch)),
      takeUntilDestroyed(this.destroyRef)
    ).subscribe()
  }

  searchProduct$(searchParam: ProductSearch) {
    this.loading = true
    this.pageInfo.page = this.offset - 1
    return this.productService.search(searchParam, this.pageInfo)
      .pipe(
        tap(res => {
          this.products = res.data
          this.totalElements = res.totalElements
        }),
        finalize(() => this.loading = false)
        );
  }

   changePrice(priceRange: number[]) {
    this.searchParams.controls.price_min.patchValue(priceRange[0] as any);
    this.searchParams.controls.price_max.patchValue(priceRange[1] as any);
   }

   changePageIndex(index: number) {
    this.offset = index;
    this.searchProduct$(this.searchParams.value as ProductSearch).subscribe();
   }

   changePageSize(size: number) {
    this.pageInfo.size = size;
    this.searchProduct$(this.searchParams.value as ProductSearch).subscribe();
   }

  openCart() {
    this.cartVisible = true
  }
  closeCart() {
    this.cartVisible = false
  }

  addProductToCart(product: Product) {
    const productIndex = this.cartItems.findIndex(item => item.product.id === product.id);
    if (productIndex > -1) {
      this.cartItems[productIndex].quantity++;
      this.updateItemPrice(this.cartItems[productIndex])
    } else {
      this.cartItems.unshift({
        product,
        quantity: 1,
        total_price: product.price
      })
    }
    this.calculateTotalPriceInCart()
    this.message.success(`${product.name} is added to cart`)
  }

  addQuantity(item: CartItem) {
    item.quantity++
    this.updateItemPrice(item)
  }

  decreaseQuantity(item: CartItem) {
    item.quantity--
    this.updateItemPrice(item)
  }

  updateItemPrice(item: CartItem) {
    item.total_price = item.product.price * item.quantity
    this.calculateTotalPriceInCart()
  }

  removeItem(selectedItem: CartItem) {
    this.cartItems = this.cartItems.filter(item => item.product.id != selectedItem.product.id)
    this.calculateTotalPriceInCart()
    this.message.success(`${selectedItem.product.name} has been removed`)
  }

  calculateTotalPriceInCart() {
    this.totalPriceInCart = this.cartItems.reduce(
      (accumulator, currentValue) => accumulator + currentValue.total_price,
      0,
    );
    localStorage.setItem("cartItems", JSON.stringify(this.cartItems))
  }
}
