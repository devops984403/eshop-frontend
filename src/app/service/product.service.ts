import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CustomResponseEntity, Page, Product, ProductSearch} from "../model";
import {Observable} from "rxjs";
import {environment} from "../../assets/environment/environment";
import {createRequestOption} from "../util/http-reques.util";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http: HttpClient) {
  }

  search(searchParams: ProductSearch, page: Page): Observable<CustomResponseEntity<Product[]>> {
    const pageable = createRequestOption(page);
    return this.http.post<CustomResponseEntity<Product[]>>(`${environment.apiUrl}/products/search`, searchParams, {
      params: pageable
    })
  }

  create(product: Product): Observable<CustomResponseEntity<Product>> {
    return this.http.post<CustomResponseEntity<Product>>(`${environment.apiUrl}/products`, product);
  }

  findById(id: number): Observable<CustomResponseEntity<Product>> {
    return this.http.get<CustomResponseEntity<Product>>(`${environment.apiUrl}/products/${id}`);
  }

  deleteById(id: number): Observable<CustomResponseEntity<string>> {
    return this.http.delete<CustomResponseEntity<string>>(`${environment.apiUrl}/products/${id}`);
  }

}
