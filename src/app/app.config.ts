import {ApplicationConfig,
  importProvidersFrom} from '@angular/core';
import {provideRouter} from '@angular/router';

import {routes} from './app.routes';
import {en_US, provideNzI18n} from 'ng-zorro-antd/i18n';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {FormsModule} from '@angular/forms';
import {provideAnimationsAsync} from '@angular/platform-browser/animations/async';
import {HttpClientModule, provideHttpClient} from '@angular/common/http';
import { provideNzIcons } from './icons-provider';
import {NzIconModule} from "ng-zorro-antd/icon";
import { IconDefinition } from '@ant-design/icons-angular';
import { ShoppingCartOutline, EyeOutline, MinusOutline, PlusOutline, DeleteOutline } from '@ant-design/icons-angular/icons';
const icons: IconDefinition[] = [ShoppingCartOutline, EyeOutline, MinusOutline, PlusOutline, DeleteOutline];

registerLocaleData(en);

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideNzI18n(en_US),
    importProvidersFrom(
      FormsModule,
      HttpClientModule,
      NzIconModule.forChild(icons)
    ),
    provideAnimationsAsync(),
    provideHttpClient(), provideNzIcons()
  ]
};
