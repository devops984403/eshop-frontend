export interface Product {
  id: number;
  name: string;
  tag_line: string;
  first_brewed: string;
  description: string;
  image_url: string;
  price: number;
  abv: number | null;
  ibu: number | null;
  target_fg: number | null;
  target_og: number | null;
  ebc: number | null;
  srm: number | null;
  ph: number | null;
  attenuation_level: number;
  volume: {
    value: number;
    unit: string
  };
  boil_volume: {
    value: number;
    unit: string
  };
  food_pairing: string[];
  brewers_tips: string;
  contributed_by: string;
}
