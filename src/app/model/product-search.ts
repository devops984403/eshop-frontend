export interface ProductSearch {
  name: string | null;
  tag_line: string | null;
  description: string | null;
  price_min: number | null;
  price_max: number | null;
  food_pairing: string | null;
}
