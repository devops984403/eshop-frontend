import {Page} from "./page";

export interface CustomResponseEntity<T> extends Page {
  code: number;
  data: T;
  errors: string[];
  totalElements: number;
}
