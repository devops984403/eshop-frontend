# Node stage to build & cache dependencies
FROM node:18.17.0 AS builder

WORKDIR /src

ENV PATH=${PATH}:./node_modules/.bin

ENV NODE_PATH=/src/node_modules

ADD package.json ./

ADD package-lock.json ./

RUN npm ci

ADD . .

RUN npm run build --prod --build-optimizer

# stage to run application

FROM nginx:alpine

COPY nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From 'builder' stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /src/dist/eshop-frontend/browser /usr/share/nginx/html

WORKDIR /

CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.template.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]

